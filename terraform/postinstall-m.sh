#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
apt -y install git jq

# hostname
hostnamectl set-hostname master --static

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux

# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

## nginx
apt -y install nginx
sed -i 's/listen 80/listen 8888/g' /etc/nginx/sites-available/default
systemctl restart nginx 
#

## Init K3S
curl -sfL https://get.k3s.io | sh -
# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
cp /var/lib/rancher/k3s/server/token /var/www/html/token
chmod 644 /var/www/html/token
#

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##

## Metrics Server
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
EOF
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                 Welcome to OSC Environment!               ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││            This is your K3S multi-node cluster.           ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
##
